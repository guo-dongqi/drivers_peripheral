/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hdf_remote_adapter_if.h"
#include "osal_mem.h"
#include "hdi_service_common.h"

using namespace std;
using namespace testing::ext;
using namespace OHOS::Audio;

namespace {
class AudioIdlHdiAdapterTest : public testing::Test {
public:
    static void SetUpTestCase(void);
    static void TearDownTestCase(void);
    void SetUp();
    void TearDown();
    struct AudioPort audioPort = {};
    static void *handle;
    static TestGetAudioManager getAudioManager;
    static TestAudioManager *manager;
    static TestAudioManagerRelease managerRelease;
    static TestAudioAdapterRelease adapterRelease;
    static TestAudioCaptureRelease captureRelease;
    static TestAudioRenderRelease renderRelease;
};

TestAudioManager *AudioIdlHdiAdapterTest::manager = nullptr;
void *AudioIdlHdiAdapterTest::handle = nullptr;
TestGetAudioManager AudioIdlHdiAdapterTest::getAudioManager = nullptr;
TestAudioManagerRelease AudioIdlHdiAdapterTest::managerRelease = nullptr;
TestAudioAdapterRelease AudioIdlHdiAdapterTest::adapterRelease = nullptr;
TestAudioCaptureRelease AudioIdlHdiAdapterTest::captureRelease = nullptr;
TestAudioRenderRelease AudioIdlHdiAdapterTest::renderRelease = nullptr;

void AudioIdlHdiAdapterTest::SetUpTestCase(void)
{
    int32_t ret = LoadFuctionSymbol(handle, getAudioManager, managerRelease, adapterRelease);
    ASSERT_EQ(HDF_SUCCESS, ret);
    captureRelease = (TestAudioCaptureRelease)(dlsym(handle, "AudioCaptureRelease"));
    ASSERT_NE(nullptr, captureRelease);
    renderRelease = (TestAudioRenderRelease)(dlsym(handle, "AudioRenderRelease"));
    ASSERT_NE(nullptr, renderRelease);
    (void)HdfRemoteGetCallingPid();
    manager = getAudioManager(IDL_SERVER_NAME.c_str());
    ASSERT_NE(nullptr, manager);
}

void AudioIdlHdiAdapterTest::TearDownTestCase(void)
{
    if (managerRelease != nullptr && manager != nullptr) {
        (void)managerRelease(manager);
    }
    if (handle != nullptr) {
        (void)dlclose(handle);
    }
}

void AudioIdlHdiAdapterTest::SetUp(void) {}
void AudioIdlHdiAdapterTest::TearDown(void)
{
    if (audioPort.portName != nullptr) {
        free(audioPort.portName);
    }
    (void) memset_s(&audioPort, sizeof(struct AudioPort), 0, sizeof(struct AudioPort));
}

/**
* @tc.name  AudioAdapterInitAllPorts_001
* @tc.desc  Test AudioAdapterInitAllPorts interface, return 0 if the ports is initialize successfully.
* @tc.type: FUNC
*/

HWTEST_F(AudioIdlHdiAdapterTest, AudioAdapterInitAllPorts_001, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    ASSERT_NE(nullptr, manager);

    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ASSERT_NE(nullptr, adapter);
    ret = adapter->InitAllPorts(adapter);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    EXPECT_EQ(HDF_SUCCESS, ret);
    adapterRelease(adapter);
}

/**
* @tc.name  AudioAdapterInitAllPorts_002
* @tc.desc  Test AudioAdapterInitAllPorts interface, return 0 if loads two adapters successfully.
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioAdapterInitAllPorts_002, TestSize.Level1)
{
    int32_t ret;
    int32_t ret2 = -1;
    struct AudioPort audioPort2 = {};
    struct IAudioAdapter *adapter1 = nullptr;
    struct IAudioAdapter *adapter2 = nullptr;
    ASSERT_NE(nullptr, manager);

    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter1, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ASSERT_NE(nullptr, adapter1);
    ret2 = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME_OUT, &adapter2, audioPort2);
    if (ret2 < 0 || adapter2 == nullptr) {
        if (audioPort2.portName != nullptr) {
            free(audioPort2.portName);
        }
        manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
        adapterRelease(adapter1);
        ASSERT_EQ(HDF_SUCCESS, ret2);
    }
    ret = adapter1->InitAllPorts(adapter1);
    EXPECT_EQ(HDF_SUCCESS, ret);

    ret2 = adapter2->InitAllPorts(adapter2);
    EXPECT_EQ(HDF_SUCCESS, ret2);

    ret = manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret2 = manager->UnloadAdapter(manager, ADAPTER_NAME_OUT.c_str());
    EXPECT_EQ(HDF_SUCCESS, ret2);

    adapterRelease(adapter1);
    adapterRelease(adapter2);
    free(audioPort2.portName);
}

/**
* @tc.name  AudioAdapterInitAllPortsNull_003
* @tc.desc  Test AudioAdapterInitAllPorts API, return -3/-4 if the parameter adapter is nullptr.
* @tc.type: FUNC
*/

HWTEST_F(AudioIdlHdiAdapterTest, AudioAdapterInitAllPortsNull_003, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioAdapter *adapterNull = nullptr;
    ASSERT_NE(nullptr, manager);

    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ASSERT_NE(nullptr, adapter);
    ret = adapter->InitAllPorts(adapterNull);
    EXPECT_EQ(ret == HDF_ERR_INVALID_PARAM || ret == HDF_ERR_INVALID_OBJECT, true);
    ret = manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    EXPECT_EQ(HDF_SUCCESS, ret);
    adapterRelease(adapter);
}

/**
* @tc.name  AudioAdapterGetPortCapability_001
* @tc.desc  Test AudioAdapterGetPortCapability,return 0 if PortType is PORT_OUT.
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioAdapterGetPortCapability_001, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = {};
    struct AudioPortCapability *capability = nullptr;
    capability = (struct AudioPortCapability*)OsalMemCalloc(sizeof(struct AudioPortCapability));
    ASSERT_NE(nullptr, capability);
    ASSERT_NE(nullptr, manager);

    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ASSERT_NE(nullptr, adapter);
    ret = adapter->InitAllPorts(adapter);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->GetPortCapability(adapter, &audioPort, capability);
    EXPECT_EQ(HDF_SUCCESS, ret);
    EXPECT_NE(nullptr, capability->formats);
    EXPECT_NE(nullptr, capability->subPorts);
    if (capability->subPorts != nullptr) {
        EXPECT_NE(nullptr, capability->subPorts->desc);
    }
    TestAudioPortCapabilityFree(capability, true);
    ret = manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    EXPECT_EQ(HDF_SUCCESS, ret);
    adapterRelease(adapter);
}

/**
* @tc.name  AudioAdapterGetPortCapability_002
* @tc.desc  Test AudioAdapterGetPortCapability,return 0 if PortType is PORT_IN.
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioAdapterGetPortCapability_002, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = {};
    struct AudioPortCapability *capability = nullptr;
    capability = (struct AudioPortCapability*)OsalMemCalloc(sizeof(struct AudioPortCapability));
    ASSERT_NE(nullptr, capability);
    ASSERT_NE(nullptr, manager);

    ret = GetLoadAdapter(manager, PORT_IN, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ASSERT_NE(nullptr, adapter);
    ret = adapter->InitAllPorts(adapter);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->GetPortCapability(adapter, &audioPort, capability);
    EXPECT_EQ(HDF_SUCCESS, ret);

    TestAudioPortCapabilityFree(capability, true);
    ret = manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    EXPECT_EQ(HDF_SUCCESS, ret);
    adapterRelease(adapter);
}

/**
* @tc.name  AudioAdapterGetPortCapabilityNull_003
* @tc.desc  Test AudioAdapterGetPortCapability, return -3/-4 if the parameter adapter is nullptr.
* @tc.type: FUNC
*/

HWTEST_F(AudioIdlHdiAdapterTest, AudioAdapterGetPortCapabilityNull_003, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioAdapter *adapterNull = nullptr;
    struct AudioPortCapability *capability = nullptr;
    capability = (struct AudioPortCapability*)OsalMemCalloc(sizeof(struct AudioPortCapability));
    ASSERT_NE(nullptr, capability);
    ASSERT_NE(nullptr, manager);

    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ASSERT_NE(nullptr, adapter);
    ret = adapter->InitAllPorts(adapter);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->GetPortCapability(adapterNull, &audioPort, capability);
    EXPECT_EQ(ret == HDF_ERR_INVALID_PARAM || ret == HDF_ERR_INVALID_OBJECT, true);

    OsalMemFree(capability);
    ret = manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    EXPECT_EQ(HDF_SUCCESS, ret);
    adapterRelease(adapter);
}

/**
* @tc.name  AudioAdapterGetPortCapabilityNull_004
* @tc.desc  Test AudioAdapterGetPortCapability, return -3 if the audioPort is nullptr,
            return -1 if the audioPort is not supported.
* @tc.type: FUNC
*/

HWTEST_F(AudioIdlHdiAdapterTest, AudioAdapterGetPortCapabilityNull_004, TestSize.Level1)
{
    int32_t ret;
    struct AudioPort *audioPortNull = nullptr;
    struct IAudioAdapter *adapter = nullptr;
    struct AudioPortCapability *capability = nullptr;
    capability = (struct AudioPortCapability*)OsalMemCalloc(sizeof(struct AudioPortCapability));
    ASSERT_NE(nullptr, capability);
    struct AudioPort audioPortError = {.dir = PORT_OUT, .portId = 9, .portName = strdup("AIP")};
    ASSERT_NE(nullptr, manager);

    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ASSERT_NE(nullptr, adapter);
    ret = adapter->InitAllPorts(adapter);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->GetPortCapability(adapter, audioPortNull, capability);
    EXPECT_EQ(HDF_ERR_INVALID_PARAM, ret);

    ret = adapter->GetPortCapability(adapter, &audioPortError, capability);
    EXPECT_EQ(HDF_FAILURE, ret);
    free(audioPortError.portName);
    OsalMemFree(capability);
    ret = manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    EXPECT_EQ(HDF_SUCCESS, ret);
    adapterRelease(adapter);
}
#ifdef AUDIO_ADM_PASSTHROUGH
/**
* @tc.name  AudioAdapterGetPortCapabilityNull_005
* @tc.desc  Test AudioAdapterGetPortCapability, return -3 if capability is nullptr.
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioAdapterGetPortCapabilityNull_005, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    struct AudioPortCapability *capabilityNull = nullptr;
    ASSERT_NE(nullptr, manager);

    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ASSERT_NE(nullptr, adapter);
    ret = adapter->InitAllPorts(adapter);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->GetPortCapability(adapter, &audioPort, capabilityNull);
    EXPECT_EQ(HDF_ERR_INVALID_PARAM, ret);

    ret = manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    EXPECT_EQ(HDF_SUCCESS, ret);
    adapterRelease(adapter);
}
#endif
/**
* @tc.name  AudioAdapterSetPassthroughMode_001
* @tc.desc  test AdapterSetPassthroughMode interface, return 0 if PortType is PORT_OUT.
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioAdapterSetPassthroughMode_001, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    AudioPortPassthroughMode modeLpcm = PORT_PASSTHROUGH_AUTO;
    ASSERT_NE(nullptr, manager);

    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ASSERT_NE(nullptr, adapter);
    ret = adapter->InitAllPorts(adapter);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->SetPassthroughMode(adapter, &audioPort, PORT_PASSTHROUGH_LPCM);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->GetPassthroughMode(adapter, &audioPort, &modeLpcm);
    EXPECT_EQ(HDF_SUCCESS, ret);
    EXPECT_EQ(PORT_PASSTHROUGH_LPCM, modeLpcm);

    ret = manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    EXPECT_EQ(HDF_SUCCESS, ret);
    adapterRelease(adapter);
}

/**
* @tc.name  AudioAdapterSetPassthroughMode_002
* @tc.desc  test AdapterSetPassthroughMode interface, return -1 if PortType is PORT_IN.
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioAdapterSetPassthroughMode_002, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    ASSERT_NE(nullptr, manager);

    ret = GetLoadAdapter(manager, PORT_IN, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ASSERT_NE(nullptr, adapter);
    ret = adapter->InitAllPorts(adapter);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->SetPassthroughMode(adapter, &audioPort, PORT_PASSTHROUGH_LPCM);
    EXPECT_EQ(HDF_FAILURE, ret);

    ret = manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    EXPECT_EQ(HDF_SUCCESS, ret);
    adapterRelease(adapter);
}

/**
* @tc.name  AudioAdapterSetPassthroughModeNull_003
* @tc.desc  test AdapterSetPassthroughMode interface, return -3/-4 the parameter adapter is nullptr.
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioAdapterSetPassthroughModeNull_003, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioAdapter *adapterNull = nullptr;
    ASSERT_NE(nullptr, manager);

    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ASSERT_NE(nullptr, adapter);
    ret = adapter->InitAllPorts(adapter);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->SetPassthroughMode(adapterNull, &audioPort, PORT_PASSTHROUGH_LPCM);
    EXPECT_EQ(ret == HDF_ERR_INVALID_PARAM || ret == HDF_ERR_INVALID_OBJECT, true);
    ret = manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    EXPECT_EQ(HDF_SUCCESS, ret);
    adapterRelease(adapter);
}

/**
* @tc.name  AudioAdapterSetPassthroughModeNull_004
* @tc.desc  test AdapterSetPassthroughMode interface, return -3 if the audioPort is nullptr,
            return -1 if the audioPort is not supported.
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioAdapterSetPassthroughModeNull_004, TestSize.Level1)
{
    int32_t ret;
    struct AudioPort *audioPortNull = nullptr;
    AudioPortPassthroughMode mode = PORT_PASSTHROUGH_LPCM;
    struct IAudioAdapter *adapter = nullptr;
    struct AudioPort audioPortError = { .dir = PORT_OUT, .portId = 8, .portName = strdup("AIP1")};
    ASSERT_NE(nullptr, manager);

    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ASSERT_NE(nullptr, adapter);
    ret = adapter->InitAllPorts(adapter);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->SetPassthroughMode(adapter, audioPortNull, mode);
    EXPECT_EQ(HDF_ERR_INVALID_PARAM, ret);

    ret = adapter->SetPassthroughMode(adapter, &audioPortError, mode);
    EXPECT_EQ(HDF_FAILURE, ret);
    ret = manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    EXPECT_EQ(HDF_SUCCESS, ret);
    free(audioPortError.portName);
    adapterRelease(adapter);
}

/**
* @tc.name  AudioAdapterSetPassthroughMode_005
* @tc.desc  test AdapterSetPassthroughMode interface, return -1 if the not supported mode.
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioAdapterSetPassthroughMode_005, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    ASSERT_NE(nullptr, manager);

    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ASSERT_NE(nullptr, adapter);
    ret = adapter->InitAllPorts(adapter);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->SetPassthroughMode(adapter, &audioPort, PORT_PASSTHROUGH_RAW);
    EXPECT_EQ(HDF_FAILURE, ret);

    ret = manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    EXPECT_EQ(HDF_SUCCESS, ret);
    adapterRelease(adapter);
}

/**
* @tc.name  AudioAdapterGetPassthroughMode_001
* @tc.desc  test AdapterGetPassthroughMode interface, return 0 if is get successfully.
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioAdapterGetPassthroughMode_001, TestSize.Level1)
{
    int32_t ret;
    AudioPortPassthroughMode mode = PORT_PASSTHROUGH_AUTO;
    struct IAudioAdapter *adapter = nullptr;
    ASSERT_NE(nullptr, manager);

    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ASSERT_NE(nullptr, adapter);
    ret = adapter->InitAllPorts(adapter);
    EXPECT_EQ(HDF_SUCCESS, ret);

    ret = adapter->SetPassthroughMode(adapter, &audioPort, PORT_PASSTHROUGH_LPCM);
    EXPECT_EQ(HDF_SUCCESS, ret);

    ret = adapter->GetPassthroughMode(adapter, &audioPort, &mode);
    EXPECT_EQ(HDF_SUCCESS, ret);
    EXPECT_EQ(PORT_PASSTHROUGH_LPCM, mode);

    ret = manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    EXPECT_EQ(HDF_SUCCESS, ret);
    adapterRelease(adapter);
}

/**
* @tc.name  AudioAdapterGetPassthroughModeNull_002
* @tc.desc  test AdapterGetPassthroughMode interface, return -3/-4 if the parameter adapter is nullptr..
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioAdapterGetPassthroughModeNull_002, TestSize.Level1)
{
    int32_t ret;
    AudioPortPassthroughMode mode = PORT_PASSTHROUGH_LPCM;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioAdapter *adapterNull = nullptr;
    ASSERT_NE(nullptr, manager);

    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ASSERT_NE(nullptr, adapter);
    ret = adapter->InitAllPorts(adapter);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->GetPassthroughMode(adapterNull, &audioPort, &mode);
    EXPECT_EQ(ret == HDF_ERR_INVALID_PARAM || ret == HDF_ERR_INVALID_OBJECT, true);

    ret = manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    EXPECT_EQ(HDF_SUCCESS, ret);
    adapterRelease(adapter);
}

/**
* @tc.name  AudioAdapterGetPassthroughModeNull_003
* @tc.desc  test AdapterGetPassthroughMode interface, return -3 if the audioPort is nullptr,
            return -1 if the audioPort is not supported.
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioAdapterGetPassthroughModeNull_003, TestSize.Level1)
{
    int32_t ret;
    struct AudioPort *audioPortNull = nullptr;
    AudioPortPassthroughMode mode = PORT_PASSTHROUGH_LPCM;
    struct IAudioAdapter *adapter = nullptr;
    struct AudioPort audioPortError = { .dir = PORT_OUT, .portId = 8, .portName = strdup("AIP")};
    ASSERT_NE(nullptr, manager);

    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ASSERT_NE(nullptr, adapter);
    ret = adapter->InitAllPorts(adapter);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->GetPassthroughMode(adapter, audioPortNull, &mode);
    EXPECT_EQ(HDF_ERR_INVALID_PARAM, ret);

    ret = adapter->GetPassthroughMode(adapter, &audioPortError, &mode);
    EXPECT_EQ(HDF_FAILURE, ret);
    ret = manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    EXPECT_EQ(HDF_SUCCESS, ret);
    free(audioPortError.portName);
    adapterRelease(adapter);
}

/**
* @tc.name  AudioAdapterGetPassthroughModeNull_004
* @tc.desc  test AdapterGetPassthroughMode interface, return -3 if the parameter mode is nullptr.
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioAdapterGetPassthroughModeNull_004, TestSize.Level1)
{
    int32_t ret;
    AudioPortPassthroughMode *modeNull = nullptr;
    struct IAudioAdapter *adapter = nullptr;
    ASSERT_NE(nullptr, manager);

    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ASSERT_NE(nullptr, adapter);
    ret = adapter->InitAllPorts(adapter);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->GetPassthroughMode(adapter, &audioPort, modeNull);
    EXPECT_EQ(HDF_ERR_INVALID_PARAM, ret);

    ret = manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    EXPECT_EQ(HDF_SUCCESS, ret);
    adapterRelease(adapter);
}
/**
* @tc.name  AudioCreateCapture_001
* @tc.desc  Test AudioCreateCapture interface,Returns 0 if the IAudioCapture object is created successfully
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioCreateCapture_001, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioCapture *capture = nullptr;
    ASSERT_NE(nullptr, manager);
    ret = AudioCreateCapture(manager, PIN_IN_MIC, ADAPTER_NAME, &adapter, &capture);
    EXPECT_EQ(HDF_SUCCESS, ret);
    adapter->DestroyCapture(adapter);
    captureRelease(capture);
    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
}

/**
* @tc.name  AudioCreateCapture_002
* @tc.desc  test AudioCreateCapture interface:
     (1)service mode:Returns 0,if the IAudioCapture object can be created successfully which was created
     (2)passthrough mode: Returns -1,if the IAudioCapture object can't be created which was created
* @tc.type: FUNC
*/

HWTEST_F(AudioIdlHdiAdapterTest, AudioCreateCapture_002, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioCapture *firstCapture = nullptr;
    struct IAudioCapture *secondCapture = nullptr;
    struct AudioSampleAttributes attrs = {};
    struct AudioDeviceDescriptor DevDesc = {};
    ASSERT_NE(nullptr, manager);

    ret = GetLoadAdapter(manager, PORT_IN, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    InitAttrs(attrs);
    InitDevDesc(DevDesc, audioPort.portId, PIN_IN_MIC);
    ret = adapter->CreateCapture(adapter, &DevDesc, &attrs, &firstCapture);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->CreateCapture(adapter, &DevDesc, &attrs, &secondCapture);
    EXPECT_EQ(HDF_FAILURE, ret);
    adapter->DestroyCapture(adapter);
    captureRelease(firstCapture);
    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
    free(DevDesc.desc);
}

/**
* @tc.name  AudioCreateCapture_003
* @tc.desc  test AudioCreateCapture interface,Returns 0 if the IAudioCapture object can be created successfully
    when IAudioRender was created
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioCreateCapture_003, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioRender *render = nullptr;
    struct IAudioCapture *capture = nullptr;
    struct AudioSampleAttributes attrs = {};
    struct AudioDeviceDescriptor renderDevDesc = {};
    struct AudioDeviceDescriptor captureDevDesc = {};
    ASSERT_NE(nullptr, manager);

    ret = GetLoadAdapter(manager, PORT_IN, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    InitAttrs(attrs);
    InitDevDesc(renderDevDesc, audioPort.portId, PIN_OUT_SPEAKER);
    InitDevDesc(captureDevDesc, audioPort.portId, PIN_IN_MIC);
    ret = adapter->CreateRender(adapter, &renderDevDesc, &attrs, &render);
    EXPECT_EQ(HDF_SUCCESS, ret);

    ret = adapter->CreateCapture(adapter, &captureDevDesc, &attrs, &capture);
    EXPECT_EQ(HDF_SUCCESS, ret);
    adapter->DestroyCapture(adapter);
    captureRelease(capture);
    adapter->DestroyRender(adapter);
    renderRelease(render);
    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
    free(renderDevDesc.desc);
    free(captureDevDesc.desc);
}

/**
* @tc.name  AudioCreateCaptureNull_005
* @tc.desc  Test AudioCreateCapture interface,Returns -3/-4 if the incoming parameter adapter is nullptr
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioCreateCaptureNull_005, TestSize.Level1)
{
    int32_t ret;
    struct AudioDeviceDescriptor devDesc = {};
    struct AudioSampleAttributes attrs = {};
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioAdapter *adapterNull = nullptr;
    struct IAudioCapture *capture = nullptr;
    ASSERT_NE(nullptr, manager);
    ret = GetLoadAdapter(manager, PORT_IN, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ret = InitAttrs(attrs);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = InitDevDesc(devDesc, audioPort.portId, PIN_IN_MIC);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->CreateCapture(adapterNull, &devDesc, &attrs, &capture);
    EXPECT_EQ(ret == HDF_ERR_INVALID_PARAM || ret == HDF_ERR_INVALID_OBJECT, true);
    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
    free(devDesc.desc);
}

/**
* @tc.name  AudioCreateCaptureNull_006
* @tc.desc  Test AudioCreateCapture interface,Returns -3 if the incoming parameter desc is nullptr
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioCreateCaptureNull_006, TestSize.Level1)
{
    int32_t ret;
    struct AudioSampleAttributes attrs = {};
    struct AudioDeviceDescriptor *devDesc = nullptr;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioCapture *capture = nullptr;
    ASSERT_NE(nullptr, manager);
    ret = GetLoadAdapter(manager, PORT_IN, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ret = InitAttrs(attrs);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->CreateCapture(adapter, devDesc, &attrs, &capture);
    EXPECT_EQ(HDF_ERR_INVALID_PARAM, ret);
    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
}

/**
* @tc.name  AudioCreateCaptureNull_007
* @tc.desc  Test AudioCreateCapture interface,Returns -3 if the incoming parameter attrs is nullptr
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioCreateCaptureNull_007, TestSize.Level1)
{
    int32_t ret;
    struct AudioDeviceDescriptor devDesc = {};
    struct AudioSampleAttributes *attrs = nullptr;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioCapture *capture = nullptr;
    ASSERT_NE(nullptr, manager);
    ret = GetLoadAdapter(manager, PORT_IN, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ret = InitDevDesc(devDesc, audioPort.portId, PIN_IN_MIC);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->CreateCapture(adapter, &devDesc, attrs, &capture);
    EXPECT_EQ(HDF_ERR_INVALID_PARAM, ret);
    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
    free(devDesc.desc);
}

#ifdef AUDIO_ADM_PASSTHROUGH
/**
* @tc.name  AudioCreateCaptureNull_008
* @tc.desc  Test AudioCreateCapture interface,Returns -3/-4 if the incoming parameter capture is nullptr
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioCreateCaptureNull_008, TestSize.Level1)
{
    int32_t ret;
    struct AudioDeviceDescriptor devDesc = {};
    struct AudioSampleAttributes attrs = {};
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioCapture **capture = nullptr;
    ASSERT_NE(nullptr, manager);
    ret = GetLoadAdapter(manager, PORT_IN, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ret = InitAttrs(attrs);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = InitDevDesc(devDesc, audioPort.portId, PIN_IN_MIC);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->CreateCapture(adapter, &devDesc, &attrs, capture);
    EXPECT_EQ(ret == HDF_ERR_INVALID_PARAM || ret == HDF_ERR_INVALID_OBJECT, true);
    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
    free(devDesc.desc);
}
#endif
/**
* @tc.name  AudioCreateCapture_009
* @tc.desc  Test AudioCreateCapture interface,Returns -1 if the incoming parameter adapter which port type is PORT_OUT
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioCreateCapture_009, TestSize.Level1)
{
    int32_t ret;
    struct AudioDeviceDescriptor devDesc = {};
    struct AudioSampleAttributes attrs = {};
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioCapture *capture = nullptr;
    ASSERT_NE(nullptr, manager);
    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME_OUT, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ret = InitAttrs(attrs);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = InitDevDesc(devDesc, audioPort.portId, PIN_OUT_SPEAKER);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->CreateCapture(adapter, &devDesc, &attrs, &capture);
    EXPECT_EQ(HDF_FAILURE, ret);
    manager->UnloadAdapter(manager, ADAPTER_NAME_OUT.c_str());
    adapterRelease(adapter);
    free(devDesc.desc);
}

/**
* @tc.name  AudioCreateCapture_010
* @tc.desc  Test AudioCreateCapture interface,Returns -1 if the incoming parameter desc which portID is not configed
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioCreateCapture_010, TestSize.Level1)
{
    int32_t ret;
    struct AudioDeviceDescriptor devDesc = {};
    struct AudioSampleAttributes attrs = {};
    uint32_t portId = 12;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioCapture *capture = nullptr;
    ASSERT_NE(nullptr, manager);
    ret = GetLoadAdapter(manager, PORT_IN, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    ret = InitAttrs(attrs);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = InitDevDesc(devDesc, portId, PIN_IN_MIC);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->CreateCapture(adapter, &devDesc, &attrs, &capture);
    EXPECT_EQ(HDF_FAILURE, ret);
    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
    free(devDesc.desc);
}
/**
* @tc.name  AudioCreateRender_001
* @tc.desc  test AudioCreateRender interface,return 0 if render is created successful.
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioCreateRender_001, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioRender *render = nullptr;
    struct AudioSampleAttributes attrs = {};
    struct AudioDeviceDescriptor renderDevDesc = {};
    ASSERT_NE(nullptr, manager);
    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);
    InitAttrs(attrs);
    InitDevDesc(renderDevDesc, audioPort.portId, PIN_OUT_SPEAKER);
    ret = adapter->CreateRender(adapter, &renderDevDesc, &attrs, &render);
    EXPECT_EQ(HDF_SUCCESS, ret);
    adapter->DestroyRender(adapter);
    renderRelease(render);
    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
    free(renderDevDesc.desc);
}

/**
    * @tc.name  AudioCreateRender_003
    * @tc.desc  test AudioCreateRender interface,return -1 if the incoming parameter pins is PIN_IN_MIC.
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioCreateRender_003, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioRender *render = nullptr;
    struct AudioSampleAttributes attrs = {};
    struct AudioDeviceDescriptor devDesc = {};
    ASSERT_NE(nullptr, manager);
    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);

    InitAttrs(attrs);
    InitDevDesc(devDesc, audioPort.portId, PIN_IN_MIC);

    ret = adapter->CreateRender(adapter, &devDesc, &attrs, &render);
    EXPECT_EQ(HDF_FAILURE, ret);

    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
    free(devDesc.desc);
}

/**
* @tc.name  AudioCreateRender_004
* @tc.desc  test AudioCreateRender interface,return -1 if the incoming parameter attr is error.
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioCreateRender_004, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioRender *render = nullptr;
    struct AudioSampleAttributes attrs = {};
    struct AudioDeviceDescriptor devDesc = {};
    uint32_t channelCountErr = 5;
    ASSERT_NE(nullptr, manager);
    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);

    InitAttrs(attrs);
    InitDevDesc(devDesc, audioPort.portId, PIN_OUT_SPEAKER);
    attrs.format = AUDIO_FORMAT_AAC_MAIN;
    ret = adapter->CreateRender(adapter, &devDesc, &attrs, &render);
    EXPECT_EQ(HDF_FAILURE, ret);
    attrs.channelCount = channelCountErr;
    ret = adapter->CreateRender(adapter, &devDesc, &attrs, &render);
    EXPECT_EQ(HDF_FAILURE, ret);
    attrs.type = AUDIO_IN_COMMUNICATION;
    ret = adapter->CreateRender(adapter, &devDesc, &attrs, &render);
    EXPECT_EQ(HDF_FAILURE, ret);

    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
    free(devDesc.desc);
}

/**
* @tc.name  AudioCreateRenderNull_005
* @tc.desc  test AudioCreateRender interface,Returns -3/-4 if the incoming parameter adapter is nullptr.
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioCreateRenderNull_005, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioRender *render = nullptr;
    struct IAudioAdapter *adapterNull = nullptr;
    struct AudioSampleAttributes attrs = {};
    struct AudioDeviceDescriptor devDesc = {};
    ASSERT_NE(nullptr, manager);
    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);

    InitAttrs(attrs);
    InitDevDesc(devDesc, audioPort.portId, PIN_OUT_SPEAKER);

    ret = adapter->CreateRender(adapterNull, &devDesc, &attrs, &render);
    EXPECT_EQ(ret == HDF_ERR_INVALID_PARAM || ret == HDF_ERR_INVALID_OBJECT, true);

    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
    free(devDesc.desc);
}

/**
* @tc.name  AudioCreateRenderNull_006
* @tc.desc  test AudioCreateRender interface,Returns -3 if the incoming parameter devDesc is nullptr.
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioCreateRenderNull_006, TestSize.Level1)
{
    int32_t ret;
    struct IAudioRender *render = nullptr;
    struct IAudioAdapter *adapter = nullptr;
    struct AudioSampleAttributes attrs = {};
    struct AudioDeviceDescriptor *devDescNull = nullptr;
    ASSERT_NE(nullptr, manager);
    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);

    InitAttrs(attrs);

    ret = adapter->CreateRender(adapter, devDescNull, &attrs, &render);
    EXPECT_EQ(HDF_ERR_INVALID_PARAM, ret);

    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
}

/**
* @tc.name  AudioCreateRenderNull_007
* @tc.desc  test AudioCreateRender interface,Returns -3 if the incoming parameter attrs is nullptr.
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioCreateRenderNull_007, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioRender *render = nullptr;
    struct AudioSampleAttributes *attrsNull = nullptr;
    struct AudioDeviceDescriptor devDesc = {};
    ASSERT_NE(nullptr, manager);
    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);

    InitDevDesc(devDesc, audioPort.portId, PIN_OUT_SPEAKER);

    ret = adapter->CreateRender(adapter, &devDesc, attrsNull, &render);
    EXPECT_EQ(HDF_ERR_INVALID_PARAM, ret);

    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
    free(devDesc.desc);
}

#ifdef AUDIO_ADM_PASSTHROUGH
/**
* @tc.name  AudioCreateRenderNull_008
* @tc.desc  test AudioCreateRender interface,Returns -3/-4 if the incoming parameter render is nullptr.
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioCreateRenderNull_008, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioRender **renderNull = nullptr;
    struct AudioSampleAttributes attrs = {};
    struct AudioDeviceDescriptor devDesc = {};
    ASSERT_NE(nullptr, manager);
    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);

    InitAttrs(attrs);
    InitDevDesc(devDesc, audioPort.portId, PIN_OUT_SPEAKER);

    ret = adapter->CreateRender(adapter, &devDesc, &attrs, renderNull);
    EXPECT_EQ(ret == HDF_ERR_INVALID_PARAM || ret == HDF_ERR_INVALID_OBJECT, true);

    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
    free(devDesc.desc);
}
#endif

/**
* @tc.name  AudioCreateRender_009
* @tc.desc  test AudioCreateRender interface,Returns -1 if the incoming parameter devDesc is error.
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioCreateRender_009, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioRender *render = nullptr;
    struct AudioSampleAttributes attrs = {};
    struct AudioDeviceDescriptor devDesc = {};
    ASSERT_NE(nullptr, manager);
    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);

    ret = InitAttrs(attrs);
    InitDevDesc(devDesc, audioPort.portId, PIN_OUT_SPEAKER);

    devDesc.portId = -5;
    ret = adapter->CreateRender(adapter, &devDesc, &attrs, &render);
    EXPECT_EQ(HDF_FAILURE, ret);
    devDesc.pins = PIN_NONE;
    ret = adapter->CreateRender(adapter, &devDesc, &attrs, &render);
    EXPECT_EQ(HDF_FAILURE, ret);
    free(devDesc.desc);
    devDesc.desc = strdup("devtestname");
    ret = adapter->CreateRender(adapter, &devDesc, &attrs, &render);
    EXPECT_EQ(HDF_FAILURE, ret);

    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
    free(devDesc.desc);
}

/**
* @tc.name  AudioCreateRender_010
* @tc.desc  test AudioCreateRender interface,Returns -1 if the incoming desc which portID is not configed
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioCreateRender_010, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioRender *render = nullptr;
    struct AudioSampleAttributes attrs = {};
    struct AudioDeviceDescriptor devDesc = {};
    uint32_t portId = 10;
    ASSERT_NE(nullptr, manager);
    ret = GetLoadAdapter(manager, PORT_OUT, ADAPTER_NAME, &adapter, audioPort);
    ASSERT_EQ(HDF_SUCCESS, ret);

    ret = InitAttrs(attrs);
    InitDevDesc(devDesc, portId, PIN_OUT_SPEAKER);

    ret = adapter->CreateRender(adapter, &devDesc, &attrs, &render);
    EXPECT_EQ(HDF_FAILURE, ret);
    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
    free(devDesc.desc);
}

/**
* @tc.name  AudioDestroyCapture_001
* @tc.desc  Test AudioDestroyCapture interface,Returns 0 if the IAudioCapture object is destroyed
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioDestroyCapture_001, TestSize.Level1)
{
    int32_t ret;
    AudioPortPin pins = PIN_IN_MIC;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioCapture *capture = nullptr;
    ASSERT_NE(nullptr, manager);
    ret = AudioCreateCapture(manager, pins, ADAPTER_NAME, &adapter, &capture);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret =adapter->DestroyCapture(adapter);
    EXPECT_EQ(HDF_SUCCESS, ret);
    captureRelease(capture);
    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
}

/**
* @tc.name  AudioDestroyCaptureNull_002
* @tc.desc  Test AudioDestroyCapture interface,Returns -3/-4 if the incoming parameter adapter is nullptr
* @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioDestroyCaptureNull_002, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioAdapter *adapterNull = nullptr;
    struct IAudioCapture *capture = nullptr;
    ASSERT_NE(nullptr, manager);
    ret = AudioCreateCapture(manager, PIN_IN_MIC, ADAPTER_NAME, &adapter, &capture);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->DestroyCapture(adapterNull);
    EXPECT_EQ(ret == HDF_ERR_INVALID_PARAM || ret == HDF_ERR_INVALID_OBJECT, true);
    ret = adapter->DestroyCapture(adapter);
    EXPECT_EQ(HDF_SUCCESS, ret);
    captureRelease(capture);
    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
}

/**
    * @tc.name  AudioDestroyRender_001
    * @tc.desc  Test AudioDestroyRender interface, return 0 if render is destroyed successful.
    * @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioDestroyRender_001, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioRender *render = nullptr;
    ASSERT_NE(nullptr, manager);
    ret = AudioCreateRender(manager, PIN_OUT_SPEAKER, ADAPTER_NAME, &adapter, &render);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->DestroyRender(adapter);
    EXPECT_EQ(HDF_SUCCESS, ret);
    renderRelease(render);
    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
}
/**
    * @tc.name  AudioDestroyRenderNull_002
    * @tc.desc  Test AudioDestroyRender interface, return -3/-4 if the parameter render is nullptr.
    * @tc.type: FUNC
*/
HWTEST_F(AudioIdlHdiAdapterTest, AudioDestroyRenderNull_002, TestSize.Level1)
{
    int32_t ret;
    struct IAudioAdapter *adapter = nullptr;
    struct IAudioRender *render = nullptr;
    struct IAudioAdapter *adapterNull = nullptr;
    ASSERT_NE(nullptr, manager);
    ret = AudioCreateRender(manager, PIN_OUT_SPEAKER, ADAPTER_NAME, &adapter, &render);
    EXPECT_EQ(HDF_SUCCESS, ret);
    ret = adapter->DestroyRender(adapterNull);
    EXPECT_EQ(ret == HDF_ERR_INVALID_PARAM || ret == HDF_ERR_INVALID_OBJECT, true);
    ret = adapter->DestroyRender(adapter);
    EXPECT_EQ(HDF_SUCCESS, ret);
    renderRelease(render);
    manager->UnloadAdapter(manager, ADAPTER_NAME.c_str());
    adapterRelease(adapter);
}
}